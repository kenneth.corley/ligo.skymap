astroplan
astropy-healpix>=0.3  # https://github.com/astropy/astropy-healpix/pull/106
astropy>=3.0
healpy
h5py
lalsuite>=6.49
lscsoft-glue
ligo-gracedb>=2.0.1
ligo-segments<1.1.0  # FIXME: non-namespace package in ligo-common dependency of ligo-segments>=1.1.0 breaks CI
matplotlib>=2.1.0
networkx
numpy>=1.15.4
pillow>=2.5.0
ptemcee
reproject>=0.3.2
scipy>=0.14
seaborn>=0.8.0
tqdm
pytz
